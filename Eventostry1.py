
import json
from datetime import datetime, timedelta

class Evento:
    def __init__(self, titulo, descripcion, fecha, hora_inicio, hora_fin):
        self.titulo = titulo
        self.descripcion = descripcion
        self.fecha = fecha
        self.hora_inicio = hora_inicio
        self.hora_fin = hora_fin
    
    def mostrar_info(self):
        print("Titulo:", self.titulo)
        print("Descripci�n:", self.descripcion)
        print("Fecha:", self.fecha)
        print("Hora de inicio:", self.hora_inicio)
        print("Hora de finalizaci�n:", self.hora_fin)
        print("=" * 30)

class Agenda:
    def __init__(self):
        self.eventos = []

    def agregar_evento(self, evento):
        if self.verificar_traslapes(evento):
            self.eventos.append(evento)
            print("Evento agregado con �xito.")
        else:
            print("No se puede agregar el evento debido a un traslape.")

    def mostrar_eventos(self):
        for evento in self.eventos:
            evento.mostrar_info()

    def buscar_eventos_por_fecha(self, fecha):
        print(f"Eventos programados para el {fecha}:")
        for evento in self.eventos:
            if evento.fecha == fecha:
                evento.mostrar_info()

    def verificar_traslapes(self, nuevo_evento):
        for evento in self.eventos:
            if evento.fecha == nuevo_evento.fecha:
                if (evento.hora_inicio < nuevo_evento.hora_inicio < evento.hora_fin) or \
                   (evento.hora_inicio < nuevo_evento.hora_fin < evento.hora_fin):
                    return False
        return True

    def guardar_en_archivo(self, nombre_archivo):
        with open(nombre_archivo, 'w') as archivo:
            eventos_serializados = [vars(evento) for evento in self.eventos]
            json.dump(eventos_serializados, archivo)

    def cargar_desde_archivo(self, nombre_archivo):
        with open(nombre_archivo, 'r') as archivo:
            eventos_serializados = json.load(archivo)
            for evento_serializado in eventos_serializados:
                evento = Evento(**evento_serializado)
                self.eventos.append(evento)

class Usuario:
    def __init__(self, nombre):
        self.nombre = nombre
        self.agenda = Agenda()

    def agregar_evento(self, evento):
        self.agenda.agregar_evento(evento)

def main():
    usuarios = []

    while True:
        print("\n1. Crear usuario")
        print("2. Agregar evento a usuario")
        print("3. Mostrar eventos de usuario")
        print("4. Buscar eventos por fecha para usuario")
        print("5. Salir")
        opcion = input("Selecciona una opci�n: ")

        if opcion == "1":
            nombre_usuario = input("Ingrese el nombre del usuario: ")
            usuarios.append(Usuario(nombre_usuario))
            print("Usuario creado con �xito.")

        elif opcion == "2":
            nombre_usuario = input("Ingrese el nombre del usuario: ")
            usuario_encontrado = None
            for usuario in usuarios:
                if usuario.nombre == nombre_usuario:
                    usuario_encontrado = usuario
                    break
            if usuario_encontrado:
                titulo = input("T�tulo del evento: ")
                descripcion = input("Descripci�n del evento: ")
                fecha = input("Fecha del evento (YYYY-MM-DD): ")
                hora_inicio = input("Hora de inicio (HH:MM): ")
                hora_fin = input("Hora de finalizaci�n (HH:MM): ")
                nuevo_evento = Evento(titulo, descripcion, fecha, hora_inicio, hora_fin)
                usuario_encontrado.agregar_evento(nuevo_evento)
            else:
                print("Usuario no encontrado.")

        elif opcion == "3":
            nombre_usuario = input("Ingrese el nombre del usuario: ")
            usuario_encontrado = None
            for usuario in usuarios:
                if usuario.nombre == nombre_usuario:
                    usuario_encontrado = usuario
                    break
            if usuario_encontrado:
                print(f"Eventos de {usuario_encontrado.nombre}:")
                usuario_encontrado.agenda.mostrar_eventos()
            else:
                print("Usuario no encontrado.")

        elif opcion == "4":
            nombre_usuario = input("Ingrese el nombre del usuario: ")
            usuario_encontrado = None
            for usuario in usuarios:
                if usuario.nombre == nombre_usuario:
                    usuario_encontrado = usuario
                    break
            if usuario_encontrado:
                fecha = input("Ingrese la fecha a buscar (YYYY-MM-DD): ")
                usuario_encontrado.agenda.buscar_eventos_por_fecha(fecha)
            else:
                print("Usuario no encontrado.")

        elif opcion == "5":
            print("Saliendo del programa...")
            break

if __name__ == "__main__":
    main()
